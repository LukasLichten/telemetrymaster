﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataModel.Model
{
    public class PitData
    {
        public int PitDataID { get; set; }
        public TimeSpan PitlaneTime { get; set; }
        public int LapsCompleted { get; set; }

        //Foreign Keys:
        public RealtimeScoringData RealtimeScoringData { get; set; } //PitEntry
        public int RealtimeScoringDataID { get; set; }
        public PitDataExit PitDataExit { get; set; }
    }

    public class PitDataExit
    {
        public int PitDataExitID { get; set; }
        public PitData PitData { get; set; }
        public int PitDataID { get; set; }
        public RealtimeScoringData RealtimeScoringData { get; set; }
        public int RealtimeScoringDataID { get; set; }
    }
}
