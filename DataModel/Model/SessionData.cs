﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataModel.Model
{
    public class SessionData
    {
        public int SessionDataID { get; set; }
        [Required]
        public TimeSpan Timestamp { get; set; }
        public TimeSpan TimeRemaining { get; set; }
        public float AmbientTemp { get; set; }
        public float TrackTemp { get; set; }
        public float? RainStrength { get; set; }
        public float? Trackwetness { get; set; }


        //Foreign Keys
        [Required]
        public Event Event { get; set; }
        public List<RealtimeScoringData> RealtimeScoringDatas { get; set; }

    }
}
