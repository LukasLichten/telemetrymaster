﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataModel.Model
{
    //Main Active Logging Object, logging basic updating Data every interval
    public class RealtimeScoringData
    {
        public int RealtimeScoringDataID { get; set; }
        public string DriverName { get; set; }
        public int Position { get; set; }
        public int PositionInClass { get; set; }
        public float TrackpositionFraction { get; set; }
        public bool InPitlane { get; set; }
        public float PosX { get; set; }
        public float PosY { get; set; }
        public float PosZ { get; set; }
        public float Speed { get; set; }

        
        //Foreign Keys
        [Required]
        public SessionData SessionData { get; set; }
        [Required]
        public Car Car { get; set; }
        
        //Optional
        public Telemetry Telemetry { get; set; }
        public LapScoringData LapScoringData { get; set; }
        public PitData PitData { get; set; }

        public PitDataExit PitDataExit { get; set; }
    }
}
