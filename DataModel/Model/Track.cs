﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataModel.Model
{
    public class Track
    {
        public int TrackID { get; set; }
        public string Trackname { get; set; }
        public float Length { get; set; }
        public string Sim { get; set; }

        //Foreign Keys
        public List<Event> Events { get; set; }
    }
}
