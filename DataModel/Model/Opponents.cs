﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataModel.Model
{
    public class Opponents
    {
        public int OpponentsID { get; set; }
        public Event Event { get; set; }

        public int EventID { get; set; }
        public List<Car> Cars { get; set; }
    }
}
