﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataModel.Model
{
    //Scoring Data for Laptimes, written to only after that car has crossed the line
    public class LapScoringData
    {
        public int LapScoringDataID { get; set; }
        public int LapsCompleted { get; set; }
        public TimeSpan LapTime { get; set; }
        public TimeSpan Sector1 { get; set; }
        public TimeSpan Sector2 { get; set; }
        public TimeSpan Sector3 { get; set; }


        //Foreign Keys:
        public RealtimeScoringData RealtimeScoringData { get; set; }
        public int RealtimeScoringDataID { get; set; }

        public TelemetryScoringData TelemetryScoringData { get; set; }

    }
}
