﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataModel.Model
{
    public class Telemetry
    {
        public int TelemetryID { get; set; }
        public float FuelLevel { get; set; }
        public float CurrentLapConsumption { get; set; }
        public float? OilTemp { get; set; }
        public float? WaterTemp { get; set; }
        public bool Headlights { get; set; }
        public bool Pitlimiter { get; set; }
        public float BrakeBias { get; set; }
        public int? TC { get; set; }
        public int? ABS { get; set; }
        public string Compound { get; set; }

        
        //Quad Data
        public float TyreWearFL { get; set; }
        public float TyreWearFR { get; set; }
        public float TyreWearRL { get; set; }
        public float TyreWearRR { get; set; }

        public float TyreTempFL { get; set; }
        public float TyreTempFR { get; set; }
        public float TyreTempRL { get; set; }
        public float TyreTempRR { get; set; }

        public float TyrePressureFL { get; set; }
        public float TyrePressureFR { get; set; }
        public float TyrePressureRL { get; set; }
        public float TyrePressureRR { get; set; }

        public float BrakeTempFL { get; set; }
        public float BrakeTempFR { get; set; }
        public float BrakeTempRL { get; set; }
        public float BrakeTempRR { get; set; }



        //Foreign Key
        public RealtimeScoringData RealtimeScoringData { get; set; }
        public int RealtimeScoringDataID { get; set; }
    }
}
