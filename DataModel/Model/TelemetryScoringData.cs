﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataModel.Model
{
    //Extended Data for when it is the players car, written to every Lap
    public class TelemetryScoringData
    {
        public int TelemetryScoringDataID { get; set; }
        public float FuelPerLap { get; set; }
        public float LastLapFuelConsumption { get; set; }
        public float FuelCapacity { get; set; }


        //Foreign Keys:
        [Required]
        public LapScoringData LapScoringData { get; set; }
        public int LapScoringDataID { get; set; }
    }
}
