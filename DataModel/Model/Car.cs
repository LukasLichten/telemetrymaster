﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataModel.Model
{
    public class Car
    {
        public int CarID { get; set; }
        public int Number { get; set; }
        public string EntryName { get; set; }
        public string Class { get; set; }
        public string ModelName { get; set; }

        //Foreign Keys:
        public Event Event { get; set; }
        public int EventID { get; set; }
        public Opponents Opponents { get; set; }
        public List<RealtimeScoringData> RealtimeScoringDatas { get; set; }
    }
}
