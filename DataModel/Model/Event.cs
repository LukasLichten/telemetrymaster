﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataModel.Model
{
    public class Event
    {
        public int EventID { get; set; }
        public string ServerIP { get; set; }
        public string SessionType { get; set; }
        public TimeSpan EventLengthTime { get; set; }
        public int EventLengthLaps { get; set; }


        //Foreign Keys
        public User User { get; set; }
        public Track Track { get; set; }
        public List<SessionData> SessionDatas { get; set; }
        public Car Car { get; set; }
        public Opponents Opponents { get; set; }
    }
}
