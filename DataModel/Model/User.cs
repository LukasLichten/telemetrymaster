﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel.Model
{
    public class User
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }

        //Need to be Unique
        public string Token { get; set; }
        public string Email { get; set; }
        
        //Foreign Keys
        public List<Event> Events { get; set; }
    }
}
