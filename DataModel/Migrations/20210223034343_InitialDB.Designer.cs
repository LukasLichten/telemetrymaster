﻿// <auto-generated />
using System;
using DataModel.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace DataModel.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20210223034343_InitialDB")]
    partial class InitialDB
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.3")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataModel.Model.Car", b =>
                {
                    b.Property<int>("CarID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Class")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("EntryName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("EventID")
                        .HasColumnType("int");

                    b.Property<string>("ModelName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Number")
                        .HasColumnType("int");

                    b.Property<int?>("OpponentsID")
                        .HasColumnType("int");

                    b.HasKey("CarID");

                    b.HasIndex("EventID")
                        .IsUnique();

                    b.HasIndex("OpponentsID");

                    b.ToTable("Cars");
                });

            modelBuilder.Entity("DataModel.Model.Event", b =>
                {
                    b.Property<int>("EventID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("EventLengthLaps")
                        .HasColumnType("int");

                    b.Property<TimeSpan>("EventLengthTime")
                        .HasColumnType("time");

                    b.Property<string>("ServerIP")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("SessionType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("TrackID")
                        .HasColumnType("int");

                    b.Property<int?>("UserID")
                        .HasColumnType("int");

                    b.HasKey("EventID");

                    b.HasIndex("TrackID");

                    b.HasIndex("UserID");

                    b.ToTable("Events");
                });

            modelBuilder.Entity("DataModel.Model.LapScoringData", b =>
                {
                    b.Property<int>("LapScoringDataID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<TimeSpan>("LapTime")
                        .HasColumnType("time");

                    b.Property<int>("LapsCompleted")
                        .HasColumnType("int");

                    b.Property<int>("RealtimeScoringDataID")
                        .HasColumnType("int");

                    b.Property<TimeSpan>("Sector1")
                        .HasColumnType("time");

                    b.Property<TimeSpan>("Sector2")
                        .HasColumnType("time");

                    b.Property<TimeSpan>("Sector3")
                        .HasColumnType("time");

                    b.HasKey("LapScoringDataID");

                    b.HasIndex("RealtimeScoringDataID")
                        .IsUnique();

                    b.ToTable("LapScoringDatas");
                });

            modelBuilder.Entity("DataModel.Model.Opponents", b =>
                {
                    b.Property<int>("OpponentsID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("EventID")
                        .HasColumnType("int");

                    b.HasKey("OpponentsID");

                    b.HasIndex("EventID")
                        .IsUnique();

                    b.ToTable("Opponents");
                });

            modelBuilder.Entity("DataModel.Model.PitData", b =>
                {
                    b.Property<int>("PitDataID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("LapsCompleted")
                        .HasColumnType("int");

                    b.Property<TimeSpan>("PitlaneTime")
                        .HasColumnType("time");

                    b.Property<int>("RealtimeScoringDataID")
                        .HasColumnType("int");

                    b.HasKey("PitDataID");

                    b.HasIndex("RealtimeScoringDataID")
                        .IsUnique();

                    b.ToTable("PitDatas");
                });

            modelBuilder.Entity("DataModel.Model.PitDataExit", b =>
                {
                    b.Property<int>("PitDataExitID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("PitDataID")
                        .HasColumnType("int");

                    b.Property<int>("RealtimeScoringDataID")
                        .HasColumnType("int");

                    b.HasKey("PitDataExitID");

                    b.HasIndex("PitDataID")
                        .IsUnique();

                    b.HasIndex("RealtimeScoringDataID")
                        .IsUnique();

                    b.ToTable("PitDataExit");
                });

            modelBuilder.Entity("DataModel.Model.RealtimeScoringData", b =>
                {
                    b.Property<int>("RealtimeScoringDataID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CarID")
                        .HasColumnType("int");

                    b.Property<string>("DriverName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("InPitlane")
                        .HasColumnType("bit");

                    b.Property<float>("PosX")
                        .HasColumnType("real");

                    b.Property<float>("PosY")
                        .HasColumnType("real");

                    b.Property<float>("PosZ")
                        .HasColumnType("real");

                    b.Property<int>("Position")
                        .HasColumnType("int");

                    b.Property<int>("PositionInClass")
                        .HasColumnType("int");

                    b.Property<int>("SessionDataID")
                        .HasColumnType("int");

                    b.Property<float>("Speed")
                        .HasColumnType("real");

                    b.Property<float>("TrackpositionFraction")
                        .HasColumnType("real");

                    b.HasKey("RealtimeScoringDataID");

                    b.HasIndex("CarID");

                    b.HasIndex("SessionDataID");

                    b.ToTable("RealtimeScoringDatas");
                });

            modelBuilder.Entity("DataModel.Model.SessionData", b =>
                {
                    b.Property<int>("SessionDataID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<float>("AmbientTemp")
                        .HasColumnType("real");

                    b.Property<int>("EventID")
                        .HasColumnType("int");

                    b.Property<float?>("RainStrength")
                        .HasColumnType("real");

                    b.Property<TimeSpan>("TimeRemaining")
                        .HasColumnType("time");

                    b.Property<TimeSpan>("Timestamp")
                        .HasColumnType("time");

                    b.Property<float>("TrackTemp")
                        .HasColumnType("real");

                    b.Property<float?>("Trackwetness")
                        .HasColumnType("real");

                    b.HasKey("SessionDataID");

                    b.HasIndex("EventID");

                    b.ToTable("SessionDatas");
                });

            modelBuilder.Entity("DataModel.Model.Telemetry", b =>
                {
                    b.Property<int>("TelemetryID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("ABS")
                        .HasColumnType("int");

                    b.Property<float>("BrakeBias")
                        .HasColumnType("real");

                    b.Property<float>("BrakeTempFL")
                        .HasColumnType("real");

                    b.Property<float>("BrakeTempFR")
                        .HasColumnType("real");

                    b.Property<float>("BrakeTempRL")
                        .HasColumnType("real");

                    b.Property<float>("BrakeTempRR")
                        .HasColumnType("real");

                    b.Property<string>("Compound")
                        .HasColumnType("nvarchar(max)");

                    b.Property<float>("CurrentLapConsumption")
                        .HasColumnType("real");

                    b.Property<float>("FuelLevel")
                        .HasColumnType("real");

                    b.Property<bool>("Headlights")
                        .HasColumnType("bit");

                    b.Property<float?>("OilTemp")
                        .HasColumnType("real");

                    b.Property<bool>("Pitlimiter")
                        .HasColumnType("bit");

                    b.Property<int>("RealtimeScoringDataID")
                        .HasColumnType("int");

                    b.Property<int?>("TC")
                        .HasColumnType("int");

                    b.Property<float>("TyrePressureFL")
                        .HasColumnType("real");

                    b.Property<float>("TyrePressureFR")
                        .HasColumnType("real");

                    b.Property<float>("TyrePressureRL")
                        .HasColumnType("real");

                    b.Property<float>("TyrePressureRR")
                        .HasColumnType("real");

                    b.Property<float>("TyreTempFL")
                        .HasColumnType("real");

                    b.Property<float>("TyreTempFR")
                        .HasColumnType("real");

                    b.Property<float>("TyreTempRL")
                        .HasColumnType("real");

                    b.Property<float>("TyreTempRR")
                        .HasColumnType("real");

                    b.Property<float>("TyreWearFL")
                        .HasColumnType("real");

                    b.Property<float>("TyreWearFR")
                        .HasColumnType("real");

                    b.Property<float>("TyreWearRL")
                        .HasColumnType("real");

                    b.Property<float>("TyreWearRR")
                        .HasColumnType("real");

                    b.Property<float?>("WaterTemp")
                        .HasColumnType("real");

                    b.HasKey("TelemetryID");

                    b.HasIndex("RealtimeScoringDataID")
                        .IsUnique();

                    b.ToTable("Telemetries");
                });

            modelBuilder.Entity("DataModel.Model.TelemetryScoringData", b =>
                {
                    b.Property<int>("TelemetryScoringDataID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<float>("FuelCapacity")
                        .HasColumnType("real");

                    b.Property<float>("FuelPerLap")
                        .HasColumnType("real");

                    b.Property<int>("LapScoringDataID")
                        .HasColumnType("int");

                    b.Property<float>("LastLapFuelConsumption")
                        .HasColumnType("real");

                    b.HasKey("TelemetryScoringDataID");

                    b.HasIndex("LapScoringDataID")
                        .IsUnique();

                    b.ToTable("TelemetryScoringDatas");
                });

            modelBuilder.Entity("DataModel.Model.Track", b =>
                {
                    b.Property<int>("TrackID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<float>("Length")
                        .HasColumnType("real");

                    b.Property<string>("Sim")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Trackname")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("TrackID");

                    b.ToTable("Tracks");
                });

            modelBuilder.Entity("DataModel.Model.User", b =>
                {
                    b.Property<int>("UserID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("IsAdmin")
                        .HasColumnType("bit");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Password")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Token")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("UserID");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("DataModel.Model.Car", b =>
                {
                    b.HasOne("DataModel.Model.Event", "Event")
                        .WithOne("Car")
                        .HasForeignKey("DataModel.Model.Car", "EventID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DataModel.Model.Opponents", "Opponents")
                        .WithMany("Cars")
                        .HasForeignKey("OpponentsID");

                    b.Navigation("Event");

                    b.Navigation("Opponents");
                });

            modelBuilder.Entity("DataModel.Model.Event", b =>
                {
                    b.HasOne("DataModel.Model.Track", "Track")
                        .WithMany("Events")
                        .HasForeignKey("TrackID");

                    b.HasOne("DataModel.Model.User", "User")
                        .WithMany("Events")
                        .HasForeignKey("UserID");

                    b.Navigation("Track");

                    b.Navigation("User");
                });

            modelBuilder.Entity("DataModel.Model.LapScoringData", b =>
                {
                    b.HasOne("DataModel.Model.RealtimeScoringData", "RealtimeScoringData")
                        .WithOne("LapScoringData")
                        .HasForeignKey("DataModel.Model.LapScoringData", "RealtimeScoringDataID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("RealtimeScoringData");
                });

            modelBuilder.Entity("DataModel.Model.Opponents", b =>
                {
                    b.HasOne("DataModel.Model.Event", "Event")
                        .WithOne("Opponents")
                        .HasForeignKey("DataModel.Model.Opponents", "EventID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Event");
                });

            modelBuilder.Entity("DataModel.Model.PitData", b =>
                {
                    b.HasOne("DataModel.Model.RealtimeScoringData", "RealtimeScoringData")
                        .WithOne("PitData")
                        .HasForeignKey("DataModel.Model.PitData", "RealtimeScoringDataID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("RealtimeScoringData");
                });

            modelBuilder.Entity("DataModel.Model.PitDataExit", b =>
                {
                    b.HasOne("DataModel.Model.PitData", "PitData")
                        .WithOne("PitDataExit")
                        .HasForeignKey("DataModel.Model.PitDataExit", "PitDataID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DataModel.Model.RealtimeScoringData", "RealtimeScoringData")
                        .WithOne("PitDataExit")
                        .HasForeignKey("DataModel.Model.PitDataExit", "RealtimeScoringDataID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("PitData");

                    b.Navigation("RealtimeScoringData");
                });

            modelBuilder.Entity("DataModel.Model.RealtimeScoringData", b =>
                {
                    b.HasOne("DataModel.Model.Car", "Car")
                        .WithMany("RealtimeScoringDatas")
                        .HasForeignKey("CarID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DataModel.Model.SessionData", "SessionData")
                        .WithMany("RealtimeScoringDatas")
                        .HasForeignKey("SessionDataID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Car");

                    b.Navigation("SessionData");
                });

            modelBuilder.Entity("DataModel.Model.SessionData", b =>
                {
                    b.HasOne("DataModel.Model.Event", "Event")
                        .WithMany("SessionDatas")
                        .HasForeignKey("EventID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Event");
                });

            modelBuilder.Entity("DataModel.Model.Telemetry", b =>
                {
                    b.HasOne("DataModel.Model.RealtimeScoringData", "RealtimeScoringData")
                        .WithOne("Telemetry")
                        .HasForeignKey("DataModel.Model.Telemetry", "RealtimeScoringDataID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("RealtimeScoringData");
                });

            modelBuilder.Entity("DataModel.Model.TelemetryScoringData", b =>
                {
                    b.HasOne("DataModel.Model.LapScoringData", "LapScoringData")
                        .WithOne("TelemetryScoringData")
                        .HasForeignKey("DataModel.Model.TelemetryScoringData", "LapScoringDataID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("LapScoringData");
                });

            modelBuilder.Entity("DataModel.Model.Car", b =>
                {
                    b.Navigation("RealtimeScoringDatas");
                });

            modelBuilder.Entity("DataModel.Model.Event", b =>
                {
                    b.Navigation("Car");

                    b.Navigation("Opponents");

                    b.Navigation("SessionDatas");
                });

            modelBuilder.Entity("DataModel.Model.LapScoringData", b =>
                {
                    b.Navigation("TelemetryScoringData");
                });

            modelBuilder.Entity("DataModel.Model.Opponents", b =>
                {
                    b.Navigation("Cars");
                });

            modelBuilder.Entity("DataModel.Model.PitData", b =>
                {
                    b.Navigation("PitDataExit");
                });

            modelBuilder.Entity("DataModel.Model.RealtimeScoringData", b =>
                {
                    b.Navigation("LapScoringData");

                    b.Navigation("PitData");

                    b.Navigation("PitDataExit");

                    b.Navigation("Telemetry");
                });

            modelBuilder.Entity("DataModel.Model.SessionData", b =>
                {
                    b.Navigation("RealtimeScoringDatas");
                });

            modelBuilder.Entity("DataModel.Model.Track", b =>
                {
                    b.Navigation("Events");
                });

            modelBuilder.Entity("DataModel.Model.User", b =>
                {
                    b.Navigation("Events");
                });
#pragma warning restore 612, 618
        }
    }
}
