﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataModel.Migrations
{
    public partial class InitialDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Tracks",
                columns: table => new
                {
                    TrackID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Trackname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Length = table.Column<float>(type: "real", nullable: false),
                    Sim = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tracks", x => x.TrackID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsAdmin = table.Column<bool>(type: "bit", nullable: false),
                    Token = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserID);
                });

            migrationBuilder.CreateTable(
                name: "Events",
                columns: table => new
                {
                    EventID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ServerIP = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SessionType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EventLengthTime = table.Column<TimeSpan>(type: "time", nullable: false),
                    EventLengthLaps = table.Column<int>(type: "int", nullable: false),
                    UserID = table.Column<int>(type: "int", nullable: true),
                    TrackID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Events", x => x.EventID);
                    table.ForeignKey(
                        name: "FK_Events_Tracks_TrackID",
                        column: x => x.TrackID,
                        principalTable: "Tracks",
                        principalColumn: "TrackID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Events_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Opponents",
                columns: table => new
                {
                    OpponentsID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EventID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Opponents", x => x.OpponentsID);
                    table.ForeignKey(
                        name: "FK_Opponents_Events_EventID",
                        column: x => x.EventID,
                        principalTable: "Events",
                        principalColumn: "EventID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SessionDatas",
                columns: table => new
                {
                    SessionDataID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Timestamp = table.Column<TimeSpan>(type: "time", nullable: false),
                    TimeRemaining = table.Column<TimeSpan>(type: "time", nullable: false),
                    AmbientTemp = table.Column<float>(type: "real", nullable: false),
                    TrackTemp = table.Column<float>(type: "real", nullable: false),
                    RainStrength = table.Column<float>(type: "real", nullable: true),
                    Trackwetness = table.Column<float>(type: "real", nullable: true),
                    EventID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SessionDatas", x => x.SessionDataID);
                    table.ForeignKey(
                        name: "FK_SessionDatas_Events_EventID",
                        column: x => x.EventID,
                        principalTable: "Events",
                        principalColumn: "EventID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cars",
                columns: table => new
                {
                    CarID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Number = table.Column<int>(type: "int", nullable: false),
                    EntryName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Class = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModelName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EventID = table.Column<int>(type: "int", nullable: false),
                    OpponentsID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cars", x => x.CarID);
                    table.ForeignKey(
                        name: "FK_Cars_Events_EventID",
                        column: x => x.EventID,
                        principalTable: "Events",
                        principalColumn: "EventID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Cars_Opponents_OpponentsID",
                        column: x => x.OpponentsID,
                        principalTable: "Opponents",
                        principalColumn: "OpponentsID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RealtimeScoringDatas",
                columns: table => new
                {
                    RealtimeScoringDataID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DriverName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Position = table.Column<int>(type: "int", nullable: false),
                    PositionInClass = table.Column<int>(type: "int", nullable: false),
                    TrackpositionFraction = table.Column<float>(type: "real", nullable: false),
                    InPitlane = table.Column<bool>(type: "bit", nullable: false),
                    PosX = table.Column<float>(type: "real", nullable: false),
                    PosY = table.Column<float>(type: "real", nullable: false),
                    PosZ = table.Column<float>(type: "real", nullable: false),
                    Speed = table.Column<float>(type: "real", nullable: false),
                    SessionDataID = table.Column<int>(type: "int", nullable: false),
                    CarID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RealtimeScoringDatas", x => x.RealtimeScoringDataID);
                    table.ForeignKey(
                        name: "FK_RealtimeScoringDatas_Cars_CarID",
                        column: x => x.CarID,
                        principalTable: "Cars",
                        principalColumn: "CarID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RealtimeScoringDatas_SessionDatas_SessionDataID",
                        column: x => x.SessionDataID,
                        principalTable: "SessionDatas",
                        principalColumn: "SessionDataID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LapScoringDatas",
                columns: table => new
                {
                    LapScoringDataID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LapsCompleted = table.Column<int>(type: "int", nullable: false),
                    LapTime = table.Column<TimeSpan>(type: "time", nullable: false),
                    Sector1 = table.Column<TimeSpan>(type: "time", nullable: false),
                    Sector2 = table.Column<TimeSpan>(type: "time", nullable: false),
                    Sector3 = table.Column<TimeSpan>(type: "time", nullable: false),
                    RealtimeScoringDataID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LapScoringDatas", x => x.LapScoringDataID);
                    table.ForeignKey(
                        name: "FK_LapScoringDatas_RealtimeScoringDatas_RealtimeScoringDataID",
                        column: x => x.RealtimeScoringDataID,
                        principalTable: "RealtimeScoringDatas",
                        principalColumn: "RealtimeScoringDataID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PitDatas",
                columns: table => new
                {
                    PitDataID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PitlaneTime = table.Column<TimeSpan>(type: "time", nullable: false),
                    LapsCompleted = table.Column<int>(type: "int", nullable: false),
                    RealtimeScoringDataID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PitDatas", x => x.PitDataID);
                    table.ForeignKey(
                        name: "FK_PitDatas_RealtimeScoringDatas_RealtimeScoringDataID",
                        column: x => x.RealtimeScoringDataID,
                        principalTable: "RealtimeScoringDatas",
                        principalColumn: "RealtimeScoringDataID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Telemetries",
                columns: table => new
                {
                    TelemetryID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FuelLevel = table.Column<float>(type: "real", nullable: false),
                    CurrentLapConsumption = table.Column<float>(type: "real", nullable: false),
                    OilTemp = table.Column<float>(type: "real", nullable: true),
                    WaterTemp = table.Column<float>(type: "real", nullable: true),
                    Headlights = table.Column<bool>(type: "bit", nullable: false),
                    Pitlimiter = table.Column<bool>(type: "bit", nullable: false),
                    BrakeBias = table.Column<float>(type: "real", nullable: false),
                    TC = table.Column<int>(type: "int", nullable: true),
                    ABS = table.Column<int>(type: "int", nullable: true),
                    Compound = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TyreWearFL = table.Column<float>(type: "real", nullable: false),
                    TyreWearFR = table.Column<float>(type: "real", nullable: false),
                    TyreWearRL = table.Column<float>(type: "real", nullable: false),
                    TyreWearRR = table.Column<float>(type: "real", nullable: false),
                    TyreTempFL = table.Column<float>(type: "real", nullable: false),
                    TyreTempFR = table.Column<float>(type: "real", nullable: false),
                    TyreTempRL = table.Column<float>(type: "real", nullable: false),
                    TyreTempRR = table.Column<float>(type: "real", nullable: false),
                    TyrePressureFL = table.Column<float>(type: "real", nullable: false),
                    TyrePressureFR = table.Column<float>(type: "real", nullable: false),
                    TyrePressureRL = table.Column<float>(type: "real", nullable: false),
                    TyrePressureRR = table.Column<float>(type: "real", nullable: false),
                    BrakeTempFL = table.Column<float>(type: "real", nullable: false),
                    BrakeTempFR = table.Column<float>(type: "real", nullable: false),
                    BrakeTempRL = table.Column<float>(type: "real", nullable: false),
                    BrakeTempRR = table.Column<float>(type: "real", nullable: false),
                    RealtimeScoringDataID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Telemetries", x => x.TelemetryID);
                    table.ForeignKey(
                        name: "FK_Telemetries_RealtimeScoringDatas_RealtimeScoringDataID",
                        column: x => x.RealtimeScoringDataID,
                        principalTable: "RealtimeScoringDatas",
                        principalColumn: "RealtimeScoringDataID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TelemetryScoringDatas",
                columns: table => new
                {
                    TelemetryScoringDataID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FuelPerLap = table.Column<float>(type: "real", nullable: false),
                    LastLapFuelConsumption = table.Column<float>(type: "real", nullable: false),
                    FuelCapacity = table.Column<float>(type: "real", nullable: false),
                    LapScoringDataID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TelemetryScoringDatas", x => x.TelemetryScoringDataID);
                    table.ForeignKey(
                        name: "FK_TelemetryScoringDatas_LapScoringDatas_LapScoringDataID",
                        column: x => x.LapScoringDataID,
                        principalTable: "LapScoringDatas",
                        principalColumn: "LapScoringDataID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PitDataExit",
                columns: table => new
                {
                    PitDataExitID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PitDataID = table.Column<int>(type: "int", nullable: false),
                    RealtimeScoringDataID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PitDataExit", x => x.PitDataExitID);
                    table.ForeignKey(
                        name: "FK_PitDataExit_PitDatas_PitDataID",
                        column: x => x.PitDataID,
                        principalTable: "PitDatas",
                        principalColumn: "PitDataID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PitDataExit_RealtimeScoringDatas_RealtimeScoringDataID",
                        column: x => x.RealtimeScoringDataID,
                        principalTable: "RealtimeScoringDatas",
                        principalColumn: "RealtimeScoringDataID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cars_EventID",
                table: "Cars",
                column: "EventID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cars_OpponentsID",
                table: "Cars",
                column: "OpponentsID");

            migrationBuilder.CreateIndex(
                name: "IX_Events_TrackID",
                table: "Events",
                column: "TrackID");

            migrationBuilder.CreateIndex(
                name: "IX_Events_UserID",
                table: "Events",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_LapScoringDatas_RealtimeScoringDataID",
                table: "LapScoringDatas",
                column: "RealtimeScoringDataID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Opponents_EventID",
                table: "Opponents",
                column: "EventID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PitDataExit_PitDataID",
                table: "PitDataExit",
                column: "PitDataID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PitDataExit_RealtimeScoringDataID",
                table: "PitDataExit",
                column: "RealtimeScoringDataID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PitDatas_RealtimeScoringDataID",
                table: "PitDatas",
                column: "RealtimeScoringDataID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RealtimeScoringDatas_CarID",
                table: "RealtimeScoringDatas",
                column: "CarID");

            migrationBuilder.CreateIndex(
                name: "IX_RealtimeScoringDatas_SessionDataID",
                table: "RealtimeScoringDatas",
                column: "SessionDataID");

            migrationBuilder.CreateIndex(
                name: "IX_SessionDatas_EventID",
                table: "SessionDatas",
                column: "EventID");

            migrationBuilder.CreateIndex(
                name: "IX_Telemetries_RealtimeScoringDataID",
                table: "Telemetries",
                column: "RealtimeScoringDataID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TelemetryScoringDatas_LapScoringDataID",
                table: "TelemetryScoringDatas",
                column: "LapScoringDataID",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PitDataExit");

            migrationBuilder.DropTable(
                name: "Telemetries");

            migrationBuilder.DropTable(
                name: "TelemetryScoringDatas");

            migrationBuilder.DropTable(
                name: "PitDatas");

            migrationBuilder.DropTable(
                name: "LapScoringDatas");

            migrationBuilder.DropTable(
                name: "RealtimeScoringDatas");

            migrationBuilder.DropTable(
                name: "Cars");

            migrationBuilder.DropTable(
                name: "SessionDatas");

            migrationBuilder.DropTable(
                name: "Opponents");

            migrationBuilder.DropTable(
                name: "Events");

            migrationBuilder.DropTable(
                name: "Tracks");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
