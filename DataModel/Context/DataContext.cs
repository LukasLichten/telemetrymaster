﻿using System;
using System.Collections.Generic;
using System.Text;
using DataModel.Model;
using Microsoft.EntityFrameworkCore;

namespace DataModel.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Track> Tracks { get; set; }
        public DbSet<SessionData> SessionDatas { get; set; }
        public DbSet<RealtimeScoringData> RealtimeScoringDatas { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Telemetry> Telemetries { get; set; }
        public DbSet<LapScoringData> LapScoringDatas { get; set; }
        public DbSet<TelemetryScoringData> TelemetryScoringDatas { get; set; }
        public DbSet<PitData> PitDatas { get; set; }
    }
}
