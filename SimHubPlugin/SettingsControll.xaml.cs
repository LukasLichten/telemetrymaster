﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TelemetryMasterPlugin
{
    /// <summary>
    /// Interaction logic for SettingsControll.xaml
    /// </summary>
    public partial class SettingsControll : UserControl
    {
        private MainPlugin mainPlugin;


        public SettingsControll(MainPlugin mainPlugin)
        {
            this.mainPlugin = mainPlugin;
            InitializeComponent();

            TbServerAddress.Text = mainPlugin.SettingsData.ServerAddress;
            TbServerPort.Text = mainPlugin.SettingsData.Port + "";
            TbToken.Text = mainPlugin.SettingsData.Token;
            ChkEnabled.IsChecked = mainPlugin.SettingsData.Enabled;

            testIP();
        }

        private void BtnApply_Click(object sender, RoutedEventArgs e)
        {
            if (!testIP())
            {
                return;
            }

            try
            {
                Uri uri = new Uri("http://" + TbServerAddress.Text + ":" + TbServerPort.Text + "/ping");

                Credentials cred = new Credentials();
                cred.Token = TbToken.Text;

                var json = System.Text.Json.JsonSerializer.Serialize(cred);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                Task task = mainPlugin.Client.PostAsync(uri, content);
                task.Start();
                task.Wait();


                if (task.IsFaulted)
                {
                    throw task.Exception;
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    ex = ex.InnerException; //Removing the useless outer layer of a task exception

                if (ex.InnerException != null)
                    ex = ex.InnerException;

                ChkEnabled.Content = ex.Message;

                BtnApply.IsEnabled = false;
                ChkEnabled.IsChecked = false;
                ChkEnabled.IsEnabled = false;
            }

            mainPlugin.SettingsData.ServerAddress = TbServerAddress.Text;
            mainPlugin.SettingsData.Port = Int32.Parse(TbServerPort.Text);
            mainPlugin.SettingsData.Token = TbToken.Text;
            mainPlugin.SettingsData.Enabled = ChkEnabled.IsChecked.Value;

            BtnApply.IsEnabled = false; //changes will have to be made to enable apply again
        }

        private bool testIP()
        {
            if (TbServerAddress.Text.Trim().Equals("") || TbServerPort.Text.Trim().Equals("") || TbToken.Text.Trim().Equals(""))
            {
                BtnApply.IsEnabled = false;
                ChkEnabled.IsChecked = false;
                ChkEnabled.IsEnabled = false;

                return false;
            }
            try
            {
                Uri uri = new Uri("http://" + TbServerAddress.Text + ":" + TbServerPort.Text + "/ping");

                ChkEnabled.Content = "";

                BtnApply.IsEnabled = true;
                ChkEnabled.IsEnabled = true;
                return true;
            }
            catch (Exception ex)
            {
                ChkEnabled.Content = ex.Message;

                BtnApply.IsEnabled = false;
                ChkEnabled.IsChecked = false;
                ChkEnabled.IsEnabled = false;

                return false;
            }
        }

        private void UpdateText(object sender, TextChangedEventArgs e)
        {
            testIP();
        }

        private void ChkEnabled_Checked(object sender, RoutedEventArgs e)
        {
            testIP();
        }
    }
}
