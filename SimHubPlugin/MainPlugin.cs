﻿using GameReaderCommon;
using SimHub.Plugins;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace TelemetryMasterPlugin
{



    [PluginDescription("Plugin for Connecting and Submitting Data to TelemetryMaster")]
    [PluginAuthor("Lukas Lichten")]
    [PluginName("TelemetryMasterPlugin")]
    public class MainPlugin : IPlugin, IDataPlugin, IWPFSettings
    {


        /// <summary>
        /// Instance of the current plugin manager
        /// </summary>
        public PluginManager PluginManager { get; set; }

        public TelemetrySettingsData SettingsData { get; set; }

        public HttpClient Client { get; set; }

        private TimeSpan LastUpdateTimestamp;

        /// <summary>
        /// Called one time per game data update, contains all normalized game data, 
        /// raw data are intentionnally "hidden" under a generic object type (A plugin SHOULD NOT USE IT)
        /// 
        /// This method is on the critical path, it must execute as fast as possible and avoid throwing any error
        /// 
        /// </summary>
        /// <param name="pluginManager"></param>
        /// <param name="data"></param>
        public void DataUpdate(PluginManager pluginManager, ref GameData data)
        {
            TimeSpan timestamp = DateTime.Now.Subtract(new DateTime(1970, 1, 1));
            pluginManager.SetPropertyValue("Timestamp", this.GetType(), timestamp);




            //Doing GameData
            if (data.GameRunning && data.NewData != null && !data.GameInMenu && !data.GamePaused && SettingsData.Enabled)
            {
                TimeSpan timeSpan = this.LastUpdateTimestamp.Add(new TimeSpan(0, 0, 0, 0, 990));


                if (timestamp > timeSpan)
                {
                    LastUpdateTimestamp = timestamp;

                    SimHub.Logging.Current.Info("Triggered");

                    DataTransferModel transferModel = new DataTransferModel();

                    DataSender sender = new DataSender(this, transferModel);
                    Task.Run(() => sender.SendData());

                }

                
            }
        }

        

        public void SendRespondsHandler(Task<HttpResponseMessage> task)
        {
            if (task.IsFaulted)
            {
                //Connection failed
                PluginManager.SetPropertyValue("ConnectionStatus", this.GetType(), false);

                Exception ex = task.Exception;

                if (ex.InnerException != null)
                    ex = ex.InnerException; //Removing the useless outer layer of a task exception

                if (ex.InnerException != null)
                    ex = ex.InnerException;

                if (ex != null)
                    PluginManager.SetPropertyValue("ConnectionMessage", this.GetType(), ex.Message);
                else
                    PluginManager.SetPropertyValue("ConnectionMessage", this.GetType(), "Unknown cancelation");

                
            }
            else if (task.IsCanceled)
            {
                PluginManager.SetPropertyValue("ConnectionStatus", this.GetType(), false);
                PluginManager.SetPropertyValue("ConnectionMessage", this.GetType(), "Unknown cancelation");
                
            }
        }

        /// <summary>
        /// Called at plugin manager stop, close/dispose anything needed here ! 
        /// Plugins are rebuilt at game change
        /// </summary>
        /// <param name="pluginManager"></param>
        public void End(PluginManager pluginManager)
        {
            // Save settings
            this.SaveCommonSettings("TelemetryMasterSettings", SettingsData);
            try
            {
                Client.CancelPendingRequests();
                Client.Dispose();
            }
            catch
            {
            
            }
        }

        /// <summary>
        /// Returns the settings control, return null if no settings control is required
        /// </summary>
        /// <param name="pluginManager"></param>
        /// <returns></returns>
        public System.Windows.Controls.Control GetWPFSettingsControl(PluginManager pluginManager)
        {
            return new SettingsControll(this);
        }

        /// <summary>
        /// Called once after plugins startup
        /// Plugins are rebuilt at game change
        /// </summary>
        /// <param name="pluginManager"></param>
        public void Init(PluginManager pluginManager)
        {

            SimHub.Logging.Current.Info("Starting TelemetryMaster plugin");

            Client = new HttpClient();

            // Load settings
            SettingsData = this.ReadCommonSettings<TelemetrySettingsData>("TelemetryMasterSettings", () => new TelemetrySettingsData());


            // Declare a property available in the property list
            pluginManager.AddProperty("Timestamp", this.GetType(), TimeSpan.Zero);
            pluginManager.AddProperty("ConnectionStatus", this.GetType(), false);
            pluginManager.AddProperty("ConnectionMessage", this.GetType(), "");
        }
    }
}