﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelemetryMasterPlugin
{
    public class DataTransferModel
    {
        public Credentials Credentials { get; set; }
    }

    public class Credentials
    {
        public string Token { get; set; }
    }
}
