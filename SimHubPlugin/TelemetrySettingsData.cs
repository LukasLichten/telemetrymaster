﻿namespace TelemetryMasterPlugin
{
    /// <summary>
    /// Settings class, make sure it can be correctly serialized using JSON.net
    /// </summary>
    public class TelemetrySettingsData
    {
        public string ServerAddress { get; set; }
        public int Port { get; set; }
        public string Token { get; set; }
        public bool Enabled { get; set; }
    }
}