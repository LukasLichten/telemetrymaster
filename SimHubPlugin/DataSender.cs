﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameReaderCommon;
using SimHub.Plugins;
using System.Text.Json;
using System.Net.Http;

namespace TelemetryMasterPlugin
{
    class DataSender
    {

        private MainPlugin mainPlugin;

        private DataTransferModel data;

        public DataSender(MainPlugin mainPlugin, DataTransferModel data)
        {
            this.mainPlugin = mainPlugin;
            this.data = data;
        }

        public void SendData()
        {
            Uri uri = new Uri(mainPlugin.SettingsData.ServerAddress + ":" + mainPlugin.SettingsData.Port + "/send");

            var json = JsonSerializer.Serialize<DataTransferModel>(data);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var task = mainPlugin.Client.PostAsync(uri, content);
            task.ContinueWith(mainPlugin.SendRespondsHandler);
            task.Start();
        }
    }
}
